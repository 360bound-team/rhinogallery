# == Schema Information
#
# Table name: rhinogallery_images
#
#  id          :integer          not null, primary key
#  name        :string(255)      not null
#  description :text(65535)
#  image       :string(255)      not null
#  alt         :string(255)
#  position    :integer
#  active      :boolean
#  gallery_id  :integer
#  created_at  :datetime         not null
#  tags        :string(255)
#  rightholder :string(255)
#  selfright   :integer
#  info        :text(65535)
#

module Rhinogallery
  class Image < Rhinogallery::Common
    before_validation :set_rights
    mount_uploader :image, Rhinogallery::ImageUploader

    UPDATE_AVAILABLE_ATTRIBUTES = [:name, :alt, :description, :active, :image, :tags, :selfright, :rightholder, :position]
    RIGHTHOLDER_TYPES = {
        I18n.t(:empty) => :empty, 
        I18n.t(:self_right) => :self_right, 
        I18n.t(:stock) => :stock, 
        I18n.t(:public_domain) => :public_domain, 
    }
    acts_as_list scope: :gallery_id
    has_paper_trail

    # default_scope { order 'position' }
    scope :active, ->{ where(active: true ) }

    belongs_to :gallery, class_name: 'Rhinogallery::Gallery'
    validates :name, :length => { :in => 2..150 }
    validates :image, :gallery_id, presence: true

    def self.add_edited_fields(*fields)
      UPDATE_AVAILABLE_ATTRIBUTES.push(*fields)
    end

    private

        def set_rights
            self.rightholder = nil if self.selfright.to_i == 1
            self.selfright = nil if self.rightholder.present?
        end
  end
end
