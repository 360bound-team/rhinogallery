$(function(){
	$(document).on("click", "[data-behavior*=show_field]",  function(e) {	
		if ($(this).find("input:checkbox").prop('checked')) {
			// $(this).find("input:text").val('');
			$(this).find("input:text").hide();
			$(this).find("select").hide();
		} else {
			$(this).find("input:text").show();
			$(this).find("select").show();
		}
	});


	if($('.selfright input').is(':checked')) {
		$('.rightholder').prop( "disabled", true );
	}
	if($('.rightholder').val() != '') {
		$('.selfright input').prop( "disabled", true );
	}

	$('.rightholder').change(function(){
		if( $(this).val() != '') {
			$('.selfright input').prop( "disabled", true );
		} else {
			$('.selfright input').prop( "disabled", false );
		}
	});
	
	$('.selfright input').change(function(){
		if( $(this).is(':checked')) {
			$('.rightholder').prop( "disabled", true );
		} else {
			$('.rightholder').prop( "disabled", false );
		}
	});	
})