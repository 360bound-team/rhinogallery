# == Schema Information
#
# Table name: rhinogallery_galleries
#
#  id          :integer          not null, primary key
#  name        :string(255)      not null
#  slug        :string(255)      not null
#  description :text(65535)
#  image       :string(255)
#  position    :integer
#  active      :boolean          default(TRUE)
#  created_at  :datetime
#  updated_at  :datetime
#

module Rhinogallery
  class Gallery < Rhinogallery::Common
    mount_uploader :image, Rhinogallery::ImageUploader

    before_validation :build_slug

    extend FriendlyId
    friendly_id :slug, use: [:slugged, :finders]

    acts_as_list
    has_paper_trail

    default_scope { order('position') }
    scope :active, ->{ where(active: true ) }

    has_many :images, class_name: 'Rhinogallery::Image', dependent: :destroy

    validates :name, :length => { :in => 2..150 }
    validates :slug, :length => { :in => 2..150 }, :uniqueness => true
    # validates :image, :presence => true

    protected
        def build_slug
            if !self.slug.present?
                self.slug = Rhinoart::Utils.to_slug(self.name)
            else
                self.slug = Rhinoart::Utils.to_slug(self.slug)
            end
        end 
  end
end
