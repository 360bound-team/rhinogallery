module Rhinogallery
	class Common < ActiveRecord::Base
		self.abstract_class = true

		def last_version_change_user
			if defined?(translation) && translation.versions.last.present?
				begin
					Rhinoart::User.find translation.versions.last.whodunnit.to_i	
				rescue
				end			
			else
				begin
					Rhinoart::User.find versions.last.whodunnit.to_i	
				rescue
				end			
			end
		end

		def last_version_change_date
			if defined?(translation) && translation.versions.last.present?
				begin
					translation.versions.last.created_at
				rescue				
				end			
			else
				begin
					versions.last.created_at	
				rescue				
				end			
			end
		end
	end
end