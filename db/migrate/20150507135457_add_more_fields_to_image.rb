class AddMoreFieldsToImage < ActiveRecord::Migration
	def change
		add_column :rhinogallery_images, :tags, :string
		add_column :rhinogallery_images, :rightholder, :string
		add_column :rhinogallery_images, :selfright, :integer
		add_column :rhinogallery_images, :info, :text

		add_index :rhinogallery_images, :tags
		add_index :rhinogallery_images, :rightholder
		add_index :rhinogallery_images, :selfright
	end
end
